# AppDirect Integration Challenge

Part 1:
CRUD operations on vehicle using jersey framework.

Part 2:
Integration of AppDirect's subscription management and user management events

=====================================================================================================================================================

## This project is using :
1. spring-boot, Version 1.3.6.RELEASE
2. Java8
3. H2 database

=====================================================================================================================================================

## Details about vehicle WS :

+ To add vehicle:
URL: http://104.155.184.218:8181/myChallenge/vehicles/add
Sample JSON request: {"make":"BMW", "model":"320D", "manufacturingDate":"2012-04-23T18:25:43.511Z"}

+ To query vehicle using make and model
HTTP Method: GET
URL: http://104.155.184.218:8181/myChallenge/vehicles?make=BMW&model=320D

+ To query vehicle using id
HTTP Method: GET
URL: http://104.155.184.218:8181/myChallenge/vehicles/id/{id}

+ To update a vehicle
HTTP Method: PUT
URL: http://104.155.184.218:8181/myChallenge/vehicles/id/{id}
Sample JSON request: {"make":"BMW_NEW", "model":"320D_NEW", "manufacturingDate":"2013-04-23T18:25:43.511Z"}


+ To delete a user
HTTP Method: DELETE
URL: http://104.155.184.218:8181/myChallenge/vehicles/id/{id}

=====================================================================================================================================================

## Details about subscription events :

+ Subscription Create Notification URL
http://104.155.184.218:8181/subscription/create?eventUrl={eventUrl}

+ Subscription Change Notification URL
http://104.155.184.218:8181/subscription/change?eventUrl={eventUrl}

+ Subscription Cancel Notification URL
http://104.155.184.218:8181/subscription/cancel?eventUrl={eventUrl}

+ Subscription Status Notification URL
http://104.155.184.218:8181/subscription/status?eventUrl={eventUrl}

+ User Assignment Notification URL
http://104.155.184.218:8181/access/assign?eventUrl={eventUrl}

+ User Unassignment Notification URL
http://104.155.184.218:8181/access/unassign?eventUrl={eventUrl}

=====================================================================================================================================================