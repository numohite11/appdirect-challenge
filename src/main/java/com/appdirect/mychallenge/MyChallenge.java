package com.appdirect.mychallenge;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

/**
 * Main application class
 */
@ComponentScan
@EnableAutoConfiguration
@SpringBootApplication
public class MyChallenge extends SpringBootServletInitializer {

	public static void main(final String[] args) {
		new MyChallenge().configure(new SpringApplicationBuilder(MyChallenge.class)).run(args);
	}

	@Override
	protected SpringApplicationBuilder configure(final SpringApplicationBuilder application) {
		return application.sources(MyChallenge.class);
	}
}
