package com.appdirect.mychallenge.service;

import java.util.Date;

import com.appdirect.mychallenge.domain.entity.VehicleEntity;
import com.appdirect.mychallenge.rest.exception.VehicleValidationException;
import com.appdirect.mychallenge.service.exception.VehicleServiceException;

/**
 * Service Interface that exposes various CRUD operations that can be performed
 * on vehicle
 */
public interface VehicleService {

	/**
	 * Creates a vehicle
	 *
	 * @param make
	 *            makers of the vehicle
	 * @param model
	 *            model name of the vehicle
	 * @param manufacturingDate
	 *            manufacturing date of the vehicle
	 * @throws VehicleServiceException
	 *             thrown when vehicle creation failed
	 * @throws VehicleValidationException
	 *             in case of validation errors
	 */
	public void createVehicle(final String make, final String model, final Date manufacturingDate)
			throws VehicleServiceException, VehicleValidationException;

	/**
	 * query a vehicle
	 *
	 * @param make
	 *            makers of the vehicle
	 * @param model
	 *            model name of the vehicle
	 * @return VehicleEntity
	 * @throws VehicleServiceException
	 *             thrown when vehicle query failed
	 * @throws VehicleValidationException
	 *             in case of validation errors
	 */
	public VehicleEntity queryVehicle(final String make, final String model)
			throws VehicleServiceException, VehicleValidationException;

	/**
	 * Query a vehicle
	 *
	 * @param id
	 *            vehicle identifier
	 * @return VehicleEntity
	 * @throws VehicleServiceException
	 *             thrown when vehicle query failed
	 */
	public VehicleEntity queryVehicle(final long id) throws VehicleServiceException;

	/**
	 * Query a vehicle
	 *
	 * @param id
	 *            vehicle identifier
	 * @param make
	 *            makers of the vehicle
	 * @param model
	 *            model name of the vehicle
	 * @param manufacturingDate
	 *            manufacturing date of the vehicle
	 * @throws VehicleServiceException
	 *             throws when vehicle update failed
	 * @throws VehicleValidationException
	 *             in case of validation errors
	 */
	public void updateVehicle(final long id, final String make, final String model, final Date manufacturingDate)
			throws VehicleServiceException, VehicleValidationException;

	/**
	 * Deletes a vehicle
	 *
	 * @param id
	 *            vehicle identifier
	 * @throws VehicleServiceException
	 *             thrown if vehicle deletion failed
	 */
	public void deleteVehicle(final long id) throws VehicleServiceException;

}
