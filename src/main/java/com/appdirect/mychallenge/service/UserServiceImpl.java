package com.appdirect.mychallenge.service;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.appdirect.mychallenge.domain.UserRepository;
import com.appdirect.mychallenge.domain.UserRepositoryImpl;
import com.appdirect.mychallenge.domain.exception.UserDomainException;
import com.appdirect.mychallenge.rest.exception.UserValidationException;
import com.appdirect.mychallenge.service.exception.UserServiceException;
import com.appdirect.mychallenge.service.validator.UserValidator;

/**
 * User service implementation
 */
@Service
public class UserServiceImpl implements UserService {

	private static final Logger logger = Logger.getLogger(UserServiceImpl.class.getName());

	private final UserRepository userRepository;

	@Autowired
	public UserServiceImpl(final UserRepositoryImpl userRepositoryImpl) {
		this.userRepository = userRepositoryImpl;
	}

	@Override
	public int createUser(String openId, String firstName, String lastName, Long subscriptionId, String emailId)
			throws UserValidationException, UserServiceException {
		try {
			UserValidator.validate(openId, firstName, lastName, subscriptionId, emailId);
			return userRepository.createUserAccount(openId, firstName, lastName, subscriptionId, emailId);
		} catch (final UserDomainException e) {
			logger.severe("Unable to create the user with openId: " + openId + ", firstName :" + firstName
					+ ", lastName :" + lastName + ", emailId :" + emailId + " and subscriptionId :" + subscriptionId
					+ " \nException: " + e.getMessage() + e);
			throw new UserServiceException(
					"Unable to create the user with openId: " + openId + ", firstName :" + firstName + ", lastName :"
							+ lastName + ", emailId :" + emailId + " and subscriptionId :" + subscriptionId);
		}
	}

	@Override
	public void deleteUserBySubscriptionId(Long subscriptionId) throws UserServiceException {
		try {
			if (userRepository.deleteBySubscriptionId(subscriptionId)) {
				return;
			}
			logger.severe("Unable to delete user with subscriptionId: " + subscriptionId);
			throw new UserServiceException("Unable to delete user with subscriptionId: " + subscriptionId);
		} catch (final UserDomainException e) {
			logger.severe("Unable to delete user with subscriptionId: " + subscriptionId + " Exception: "
					+ e.getMessage() + e);
			throw new UserServiceException("Unable to delete user with subscriptionId: " + subscriptionId);
		}
	}

}
