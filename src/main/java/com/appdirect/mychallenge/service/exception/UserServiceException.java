package com.appdirect.mychallenge.service.exception;

/**
 * Service exception thrown when user operation fails
 */
public class UserServiceException extends Exception {

	private static final long serialVersionUID = -1832170375180383452L;

	/**
	 * Service exception thrown when user operation fails
	 * 
	 * @param message
	 */
	public UserServiceException(final String message) {
		super(message);
	}

}
