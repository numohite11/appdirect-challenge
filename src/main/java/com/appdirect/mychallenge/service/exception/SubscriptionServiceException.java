package com.appdirect.mychallenge.service.exception;

/**
 * Service exception thrown when subscription operation fails
 */
public class SubscriptionServiceException extends Exception {

	private static final long serialVersionUID = -7314355627499395762L;

	/**
	 * Service exception thrown when subscription operation fails
	 * 
	 * @param message
	 */
	public SubscriptionServiceException(final String message) {
		super(message);
	}

}
