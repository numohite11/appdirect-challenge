package com.appdirect.mychallenge.service.exception;

/**
 * Service exception thrown when vehicle operation fails
 */
public class VehicleServiceException extends Exception {

	private static final long serialVersionUID = -4063083198744762417L;

	/**
	 * Service exception thrown when vehicle operation fails
	 * 
	 * @param message
	 */
	public VehicleServiceException(final String message) {
		super(message);
	}

}
