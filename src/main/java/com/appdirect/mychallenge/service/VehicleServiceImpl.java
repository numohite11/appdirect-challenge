package com.appdirect.mychallenge.service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.appdirect.mychallenge.domain.VehicleRepository;
import com.appdirect.mychallenge.domain.VehicleRepositoryImpl;
import com.appdirect.mychallenge.domain.entity.VehicleEntity;
import com.appdirect.mychallenge.domain.exception.VehicleDomainException;
import com.appdirect.mychallenge.rest.exception.VehicleValidationException;
import com.appdirect.mychallenge.service.exception.VehicleServiceException;
import com.appdirect.mychallenge.service.validator.VehicleValidator;

/**
 * Vehicle service implementation
 */
@Service
public class VehicleServiceImpl implements VehicleService {

	private static final Logger logger = Logger.getLogger(VehicleServiceImpl.class.getName());

	private final VehicleRepository vehicleRepository;

	@Autowired
	public VehicleServiceImpl(final VehicleRepositoryImpl vehicleRepositoryImpl) {
		this.vehicleRepository = vehicleRepositoryImpl;
	}

	@Override
	public void createVehicle(final String make, final String model, final Date manufacturingDate)
			throws VehicleValidationException, VehicleServiceException {
		try {
			VehicleValidator.validate(make, model, manufacturingDate);
			if (vehicleRepository.createVehicle(make, model, manufacturingDate)) {
				return;
			}
			logger.severe("Unable to create the vehicle with make: " + make + ", model :" + model
					+ " and manufacturing date :" + manufacturingDate);
			throw new VehicleServiceException("Unable to create the vehicle with make: " + make + ", model :" + model
					+ "and manufacturing date :" + manufacturingDate);
		} catch (final VehicleDomainException e) {
			logger.severe("Unable to create the vehicle with make: " + make + ", model :" + model
					+ "and manufacturing date :" + manufacturingDate + " \nException: " + e.getMessage() + e);
			throw new VehicleServiceException("Unable to create the vehicle with make: " + make + ", model :" + model
					+ "and manufacturing date :" + manufacturingDate);
		}
	}

	@Override
	public VehicleEntity queryVehicle(final String make, final String model)
			throws VehicleValidationException, VehicleServiceException {
		try {
			VehicleValidator.validate(make, model);
			final List<VehicleEntity> vehicleEntities = vehicleRepository.getVehicle(make, model);
			if (vehicleEntities == null || vehicleEntities.isEmpty()) {
				logger.severe("Unable to find the vehicle with make: " + make + "and model :" + model);
				throw new VehicleServiceException(
						"Unable to find the vehicle with make: " + make + "and model :" + model);
			} else if (vehicleEntities.size() == 1) {
				return vehicleEntities.get(0);
			} else {
				logger.severe(
						"Some error occurred while querying the vehicle with make: " + make + "and model :" + model);
				throw new VehicleServiceException(
						"Some error occurred while querying the vehicle with make: " + make + "and model :" + model);
			}
		} catch (final VehicleDomainException e) {
			logger.severe("Unable to find the vehicle with make: " + make + "and model :" + model + " Exception: "
					+ e.getMessage() + e);
			throw new VehicleServiceException("Unable to find the vehicle with make: " + make + "and model :" + model);
		}
	}

	@Override
	public VehicleEntity queryVehicle(final long id) throws VehicleServiceException {
		try {
			final List<VehicleEntity> vehicleEntities = vehicleRepository.getVehicle(id);
			if (vehicleEntities == null || vehicleEntities.isEmpty()) {
				logger.severe("Unable to find the vehicle with id: " + id);
				throw new VehicleServiceException("Unable to find the vehicle with id: " + id);
			} else if (vehicleEntities.size() == 1) {
				return vehicleEntities.get(0);
			} else {
				logger.severe("Some error occurred while querying the vehicle with id: " + id);
				throw new VehicleServiceException("Some error occurred while querying the vehicle with id: " + id);
			}
		} catch (final VehicleDomainException e) {
			logger.severe("Unable to query vehicle with id: " + id + " Exception: " + e.getMessage() + e);
			throw new VehicleServiceException("Unable to q vehicle with id: " + id);
		}
	}

	@Override
	public void deleteVehicle(final long id) throws VehicleServiceException {
		try {
			if (vehicleRepository.deleteVehicle(id)) {
				return;
			}
			logger.severe("Unable to delete vehicle with id: " + id);
			throw new VehicleServiceException("Unable to delete vehicle with id: " + id);
		} catch (final VehicleDomainException e) {
			logger.severe("Unable to delete vehicle with id: " + id + " Exception: " + e.getMessage() + e);
			throw new VehicleServiceException("Unable to delete vehicle with id: " + id);
		}
	}

	@Override
	public void updateVehicle(final long id, final String make, final String model, final Date manufacturingDate)
			throws VehicleValidationException, VehicleServiceException {
		try {
			VehicleValidator.validate(make, model, manufacturingDate);
			final VehicleEntity vehicleEntity = queryVehicle(id);
			vehicleEntity.setMake(make);
			vehicleEntity.setModel(model);
			vehicleEntity.setManufacturingDate(manufacturingDate);
			vehicleEntity.setUpdatedDate(new Date());
			if (vehicleRepository.updateVehicle(vehicleEntity)) {
				return;
			}
			logger.severe("Unable to update vehicle with id: " + id);
			throw new VehicleServiceException("Unable to update vehicle with id: " + id);
		} catch (final VehicleDomainException e) {
			logger.severe("Unable to update vehicle with id: " + id + " Exception: " + e.getMessage() + e);
			throw new VehicleServiceException("Unable to update vehicle with id: " + id);
		}
	}
}
