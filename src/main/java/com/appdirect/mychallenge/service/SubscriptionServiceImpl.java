package com.appdirect.mychallenge.service;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.appdirect.mychallenge.domain.SubscriptionRepository;
import com.appdirect.mychallenge.domain.SubscriptionRepositoryImpl;
import com.appdirect.mychallenge.domain.exception.SubscriptionDomainException;
import com.appdirect.mychallenge.rest.exception.SubscriptionValidationException;
import com.appdirect.mychallenge.service.exception.SubscriptionServiceException;
import com.appdirect.mychallenge.service.validator.SubscriptionValidator;

/**
 * Subscription service implementation
 */
@Service
public class SubscriptionServiceImpl implements SubscriptionService {

	private static final Logger logger = Logger.getLogger(SubscriptionServiceImpl.class.getName());

	private final SubscriptionRepository subscriptionRepository;

	@Autowired
	public SubscriptionServiceImpl(final SubscriptionRepositoryImpl subscriptionRepositoryImpl) {
		this.subscriptionRepository = subscriptionRepositoryImpl;
	}

	@Override
	public long createSubscription(final String companyName, final String status, final String marketPlaceBaseUrl)
			throws SubscriptionValidationException, SubscriptionServiceException {
		try {
			SubscriptionValidator.validate(companyName, status, marketPlaceBaseUrl);
			return subscriptionRepository.createSubscription(companyName, status, marketPlaceBaseUrl);
		} catch (final SubscriptionDomainException e) {
			logger.severe("Unable to create the subscription with companyName: " + companyName + ", status :" + status
					+ " and marketPlaceBaseUrl :" + marketPlaceBaseUrl + " \nException: " + e.getMessage() + e);
			throw new SubscriptionServiceException("Unable to create the subscription with companyName: " + companyName
					+ ", status :" + status + " and marketPlaceBaseUrl :" + marketPlaceBaseUrl);
		}
	}

	@Override
	public void deleteSubscription(final long id) throws SubscriptionServiceException {
		try {
			if (subscriptionRepository.deleteSubscription(id)) {
				return;
			}
			logger.severe("Unable to delete subscription with id: " + id);
			throw new SubscriptionServiceException("Unable to delete subscription with id: " + id);
		} catch (final SubscriptionDomainException e) {
			logger.severe("Unable to delete subscription with id: " + id + " Exception: " + e.getMessage() + e);
			throw new SubscriptionServiceException("Unable to delete subscription with id: " + id);
		}
	}

	@Override
	public Long getSubscriptionId() {
		try {
			return subscriptionRepository.getSubscriptionId();
		} catch (final SubscriptionDomainException e) {
			logger.severe("Unable to get subscriptionid." + "  Exception: " + e.getMessage() + e);
			return null;
		}
	}

}
