package com.appdirect.mychallenge.service;

import com.appdirect.mychallenge.rest.exception.SubscriptionValidationException;
import com.appdirect.mychallenge.service.exception.SubscriptionServiceException;

/**
 * Service Interface that exposes various CRUD operations that can be performed
 * on subscription
 */
public interface SubscriptionService {

	/**
	 * creates a subscription
	 *
	 * @param companyName
	 *            company name
	 * @param status
	 *            status of the subscription
	 * @param marketPlaceBaseUrl
	 *            marketPlaceBaseUrl
	 * @throws SubscriptionServiceException
	 *             thrown when subscription creation failed
	 * @throws SubscriptionValidationException
	 *             in case of validation errors
	 */
	public long createSubscription(final String company, final String status, final String marketPlaceBaseUrl)
			throws SubscriptionValidationException, SubscriptionServiceException;

	/**
	 * Deletes a subscription
	 *
	 * @param id
	 *            subscription identifier
	 * @throws SubscriptionServiceException
	 *             thrown if subscription deletion failed
	 */
	public void deleteSubscription(final long id) throws SubscriptionServiceException;

	/**
	 * Gets subscription identifier if available
	 *
	 * @return subscriptionId
	 * @throws SubscriptionServiceException
	 *             thrown if no subscriptionId found
	 */
	public Long getSubscriptionId();

}
