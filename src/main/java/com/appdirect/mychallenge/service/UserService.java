package com.appdirect.mychallenge.service;

import com.appdirect.mychallenge.rest.exception.UserValidationException;
import com.appdirect.mychallenge.service.exception.UserServiceException;

/**
 * Service Interface that exposes various CRUD operations that can be performed
 * on user
 */
public interface UserService {

	/**
	 * creates a user
	 *
	 * @param openId
	 *            openIs
	 * @param firstName
	 *            firstName
	 * @param lastName
	 *            lastName
	 * @param subscriptionId
	 *            subscriptionId
	 * @param emailId
	 *            emailId
	 * @return user identifier
	 * @throws UserValidationException
	 *             in case of validation errors
	 * @throws UserServiceException
	 *             if user creation was not successful
	 */
	public int createUser(final String openId, final String firstName, final String lastName,
			final Long subscriptionId, final String emailId) throws UserValidationException, UserServiceException;

	/**
	 * @param subscriptionId
	 *            the subscription_id of the user we want to delete
	 * @return false if no row was deleted, true otherwise
	 * @throws UserServiceException
	 *             if error occurred during user deletion
	 */
	public void deleteUserBySubscriptionId(Long subscriptionId) throws UserServiceException;

}
