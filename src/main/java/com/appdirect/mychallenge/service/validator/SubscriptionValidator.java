package com.appdirect.mychallenge.service.validator;

import java.util.logging.Logger;

import org.springframework.util.StringUtils;

import com.appdirect.mychallenge.rest.exception.SubscriptionValidationException;

/**
 * Class to validate Vehicle requests
 */
public class SubscriptionValidator {

	private static final int MAX_SIZE_64 = 64;

	private static final Logger logger = Logger.getLogger(SubscriptionValidator.class.getName());

	/**
	 * Validates make, model and manufacturing date of a subscription
	 *
	 * @param companyName
	 *            company name
	 * @param status
	 *            status
	 * @param marketPlaceBaseUrl
	 *            marketPlaceBaseUrl
	 * @throws SubscriptionValidationException
	 *             in case of validation error
	 */
	public static void validate(final String companyName, final String status, final String marketPlaceBaseUrl)
			throws SubscriptionValidationException {
		inputStringValidator(companyName, "CompanyName");
		inputStringValidator(status, "Status");
		inputStringValidator(marketPlaceBaseUrl, "MarketPlaceBaseUrl");
	}

	private static void inputStringValidator(final String companyName, final String parameterName)
			throws SubscriptionValidationException {
		if (StringUtils.isEmpty(companyName)) {
			logger.severe(parameterName + "  cannot be empty.");
			throw new SubscriptionValidationException(parameterName + " cannot be empty.");
		}
		if (companyName.length() > MAX_SIZE_64) {
			logger.severe(parameterName + " cannot be more than 64 characters");
			throw new SubscriptionValidationException(parameterName + " cannot be more than 64 characters");
		}
	}

}
