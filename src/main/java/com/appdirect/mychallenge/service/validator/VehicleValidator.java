package com.appdirect.mychallenge.service.validator;

import java.util.Date;
import java.util.Objects;
import java.util.logging.Logger;

import org.springframework.util.StringUtils;

import com.appdirect.mychallenge.rest.exception.VehicleValidationException;

/**
 * Class to validate Vehicle requests
 */
public class VehicleValidator {

	private static final int MAX_SIZE_64 = 64;

	private static final Logger logger = Logger.getLogger(VehicleValidator.class.getName());

	/**
	 * Validates make, model and manufacturing date of a vehicle
	 *
	 * @param make
	 *            makers of the vehicle
	 * @param model
	 *            model name of the vehicle
	 * @param manufacturingDate
	 *            manufacturing date of the vehicle
	 * @throws VehicleValidationException
	 *             in case of validation error
	 */
	public static void validate(final String make, final String model, final Date manufacturingDate)
			throws VehicleValidationException {
		validate(make, model);
		if (Objects.isNull(manufacturingDate)) {
			logger.severe("Manufacturing date cannot be null.");
			throw new VehicleValidationException("Manufacturing date cannot be null.");
		}
	}

	/**
	 * Validates make and model of a vehicle
	 *
	 * @param make
	 *            maker of the vehicle
	 * @param model
	 *            model name of the vehicle
	 * @throws VehicleValidationException
	 *             in case of validation error
	 */
	public static void validate(final String make, final String model) throws VehicleValidationException {
		inputStringValidator(make, "Makers");
		inputStringValidator(model, "Model name ");
	}

	private static void inputStringValidator(final String companyName, final String parameterName)
			throws VehicleValidationException {
		if (StringUtils.isEmpty(companyName)) {
			logger.severe(parameterName + "  cannot be empty.");
			throw new VehicleValidationException(parameterName + " cannot be empty.");
		}
		if (companyName.length() > MAX_SIZE_64) {
			logger.severe(parameterName + " cannot be more than 64 characters");
			throw new VehicleValidationException(parameterName + " cannot be more than 64 characters");
		}
	}

}
