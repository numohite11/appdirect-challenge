package com.appdirect.mychallenge.service.validator;

import java.util.Objects;
import java.util.logging.Logger;

import org.springframework.util.StringUtils;

import com.appdirect.mychallenge.rest.exception.UserValidationException;

/**
 * Class to validate User requests
 */
public class UserValidator {

	private static final int MAX_SIZE_64 = 64;

	private static final Logger logger = Logger.getLogger(UserValidator.class.getName());

	public static void validate(final String openId, final String firstName, final String lastName,
			final Long subscriptionId, final String emailId) throws UserValidationException {
		inputStringValidator(openId, "OpenId");
		inputStringValidator(firstName, "FirstName");
		inputStringValidator(lastName, "LastName");
		inputStringValidator(emailId, "EmialId");
		if (Objects.isNull(subscriptionId)) {
			logger.severe("SubscriptionId cannot be null.");
			throw new UserValidationException("SubscriptionId cannot be null.");
		}
	}

	private static void inputStringValidator(final String companyName, final String parameterName)
			throws UserValidationException {
		if (StringUtils.isEmpty(companyName)) {
			logger.severe(parameterName + "  cannot be empty.");
			throw new UserValidationException(parameterName + " cannot be empty.");
		}
		if (companyName.length() > MAX_SIZE_64) {
			logger.severe(parameterName + " cannot be more than 64 characters");
			throw new UserValidationException(parameterName + " cannot be more than 64 characters");
		}
	}

}
