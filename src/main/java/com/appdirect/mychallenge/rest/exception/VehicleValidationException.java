package com.appdirect.mychallenge.rest.exception;

/**
 * Exception class if vehicle data in not valid
 */
public class VehicleValidationException extends Exception {

	private static final long serialVersionUID = -7494693220471691460L;

	public VehicleValidationException(final String message) {
		super(message);
	}

}
