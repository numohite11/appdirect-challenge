package com.appdirect.mychallenge.rest.exception;

/**
 * Exception class if user data in not valid
 */
public class UserValidationException extends Exception {

	private static final long serialVersionUID = -5497214899896335864L;

	public UserValidationException(final String message) {
		super(message);
	}

}
