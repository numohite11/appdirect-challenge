package com.appdirect.mychallenge.rest.exception;

/**
 * Exception class if vehicle data in not valid
 */
public class SubscriptionValidationException extends Exception {

	private static final long serialVersionUID = -7451691381791707974L;

	public SubscriptionValidationException(final String message) {
		super(message);
	}

}
