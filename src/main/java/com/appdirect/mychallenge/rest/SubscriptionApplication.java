package com.appdirect.mychallenge.rest;

import java.util.UUID;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.appdirect.mychallenge.domain.appdirect.AppDirectUser;
import com.appdirect.mychallenge.domain.appdirect.Company;
import com.appdirect.mychallenge.domain.appdirect.Notification;
import com.appdirect.mychallenge.domain.appdirect.Notification.Flag;
import com.appdirect.mychallenge.rest.response.CancelSubscriptionResponse;
import com.appdirect.mychallenge.rest.response.ChangeSubscriptionResponse;
import com.appdirect.mychallenge.rest.response.CreateSubscriptionResponse;
import com.appdirect.mychallenge.rest.response.ErrorCode;
import com.appdirect.mychallenge.rest.response.ErrorResponse;
import com.appdirect.mychallenge.rest.response.SubscriptionStatusResponse;
import com.appdirect.mychallenge.service.SubscriptionService;
import com.appdirect.mychallenge.service.SubscriptionServiceImpl;
import com.appdirect.mychallenge.service.UserService;
import com.appdirect.mychallenge.service.UserServiceImpl;

/**
 * Application class to perform various operations on subscription events
 */
@Path(value = "/subscription")
@Component
public class SubscriptionApplication {

	private static final Logger logger = Logger.getLogger(SubscriptionApplication.class.getName());

	private final SubscriptionService subscriptionService;

	private final UserService userService;

	@Autowired
	public SubscriptionApplication(final SubscriptionServiceImpl subscriptionServiceImpl,
			final UserServiceImpl UserServiceImpl) {
		this.subscriptionService = subscriptionServiceImpl;
		this.userService = UserServiceImpl;
	}

	/**
	 * Creates a subscription
	 * 
	 * @param url
	 *            eventUrl
	 * @return json response
	 */
	@GET
	@Produces("application/json")
	@Path(value = "/create")
	public Response createSubscription(@QueryParam(ApplicationHelper.EVENT_URL) final String url) {
		try {
			logger.info("Create subscription called with eventUrl: " + url);

			// TODO Oauth authentication using url and
			// get Notification object
			// for now creating a dummy notification
			final Notification notification = new Notification(Notification.Type.SUBSCRIPTION_ORDER, null,
					"applicationUuid", Flag.DEVELOPMENT, null, null);

			// TODO fetch appropriate appDirectUser and company
			// final AppDirectUser creator = notification.creator;
			// final Company company = notification.payload.company;

			// For now adding mock for appDirectUser and company
			final AppDirectUser creator = new AppDirectUser("uuid", "openId", "email", "firstName", "lastName",
					"language");
			final Company company = new Company("uuid", "email", "name", "123456789", "website");

			// TODO need to check for ErrorCode.USER_ALREADY_EXISTS
			// For now Mocking a scenario to return USER_NOT_FOUND
			if (StringUtils.isEmpty(url)) {
				final ErrorResponse errorResponse = new ErrorResponse(ErrorCode.USER_NOT_FOUND, "URL not passed.");
				return ApplicationHelper.buildResponse(Status.BAD_REQUEST, errorResponse);
			}

			final Long subscriptionId = subscriptionService.createSubscription(company.name, "ACTIVE", "baseUrl");
			// add an entry in user table with this subscription id
			userService.createUser(creator.openId, creator.firstName, creator.lastName, subscriptionId, creator.email);

			final CreateSubscriptionResponse response = new CreateSubscriptionResponse(UUID.randomUUID().toString(),
					"Subscription successfully created.");
			return ApplicationHelper.buildResponse(Status.CREATED, response);
		} catch (Exception ex) {
			logger.severe("Unexpected error occurred : " + ex);
			final ErrorResponse errorResponse = new ErrorResponse(ErrorCode.UNKNOWN_ERROR,
					"Unexpected error occurred.");
			return ApplicationHelper.buildResponse(Status.BAD_REQUEST, errorResponse);
		}
	}

	/**
	 * Cancels a subscription
	 * 
	 * @param url
	 *            eventUrl
	 * @return json response
	 */
	@GET
	@Produces("application/json")
	@Path(value = "/cancel")
	public Response cancelSubscription(@QueryParam(ApplicationHelper.EVENT_URL) final String url) {
		try {
			logger.info("Cancel subscription called with eventUrl: " + url);

			// TODO Oauth authentication using url and
			// get Notification object
			// for now creating a dummy notification
			final Notification notification = new Notification(Notification.Type.SUBSCRIPTION_CANCEL, null,
					"applicationUuid", Flag.DEVELOPMENT, null, null);

			// TODO need to check for ErrorCode.USER_ALREADY_EXISTS
			// For now Mocking a scenario to return USER_NOT_FOUND
			if (StringUtils.isEmpty(url)) {
				final ErrorResponse errorResponse = new ErrorResponse(ErrorCode.USER_NOT_FOUND, "URL not passed.");
				return ApplicationHelper.buildResponse(Status.BAD_REQUEST, errorResponse);
			}

			// TODO get the subscriptionId from account identifier
			// final Long subscriptionId =
			// Long.valueOf(notification.payload.account.accountIdentifier);

			// For now mocking it to delete the first found subscription
			final Long subscriptionId = subscriptionService.getSubscriptionId();
			if (subscriptionId != null) {
				userService.deleteUserBySubscriptionId(subscriptionId);
				subscriptionService.deleteSubscription(subscriptionId);
				final CancelSubscriptionResponse response = new CancelSubscriptionResponse(
						"Subscription successfully canceled.");
				return ApplicationHelper.buildResponse(Status.OK, response);
			} else {
				logger.severe("No subscription found to be cancelled.");
				final ErrorResponse errorResponse = new ErrorResponse(ErrorCode.USER_NOT_FOUND,
						"No subscription found to be cancelled.");
				return ApplicationHelper.buildResponse(Status.BAD_REQUEST, errorResponse);
			}
		} catch (Exception ex) {
			logger.severe("Unexpected error occurred : " + ex);
			final ErrorResponse errorResponse = new ErrorResponse(ErrorCode.UNKNOWN_ERROR,
					"Unexpected error occurred.");
			return ApplicationHelper.buildResponse(Status.BAD_REQUEST, errorResponse);
		}

	}

	/**
	 * Upgrades/down-grades subscription
	 * 
	 * @param url
	 *            eventUrl
	 * @return json response
	 */
	@GET
	@Produces("application/json")
	@Path(value = "/change")
	public Response changeSubscription(@QueryParam(ApplicationHelper.EVENT_URL) final String url) {
		try {
			logger.info("Change subscription called with eventUrl: " + url);
			if (StringUtils.isEmpty(url)) {
				// Mocking a scenario to return USER_NOT_FOUND
				final ErrorResponse errorResponse = new ErrorResponse(ErrorCode.USER_NOT_FOUND, "URL not passed.");
				return ApplicationHelper.buildResponse(Status.BAD_REQUEST, errorResponse);
			}
			// TODO need enhancement, for now having a NO-OP
			final ChangeSubscriptionResponse response = new ChangeSubscriptionResponse(
					"Subscription successfully changed.");
			return ApplicationHelper.buildResponse(Status.OK, response);
		} catch (Exception ex) {
			logger.severe("Unexpected error occurred : " + ex);
			final ErrorResponse errorResponse = new ErrorResponse(ErrorCode.UNKNOWN_ERROR,
					"Unexpected error occurred.");
			return ApplicationHelper.buildResponse(Status.BAD_REQUEST, errorResponse);
		}
	}

	/**
	 * Gets status of a subscription
	 * 
	 * @param url
	 *            eventUrl
	 * @return json response
	 */
	@GET
	@Produces("application/json")
	@Path(value = "/status")
	public Response status(@QueryParam(ApplicationHelper.EVENT_URL) final String url) {
		try {
			logger.info("Get status of a subscription called with eventUrl: " + url);
			if (StringUtils.isEmpty(url)) {
				// Mocking a scenario to return USER_NOT_FOUND
				final ErrorResponse errorResponse = new ErrorResponse(ErrorCode.USER_NOT_FOUND, "URL not passed.");
				return ApplicationHelper.buildResponse(Status.BAD_REQUEST, errorResponse);
			}
			// TODO need enhancement, for now having a NO-OP
			final SubscriptionStatusResponse response = new SubscriptionStatusResponse("ACTIVE",
					"Subscription is in ACTIVE state.");
			return ApplicationHelper.buildResponse(Status.OK, response);
		} catch (Exception ex) {
			logger.severe("Unexpected error occurred : " + ex);
			final ErrorResponse errorResponse = new ErrorResponse(ErrorCode.UNKNOWN_ERROR,
					"Unexpected error occurred.");
			return ApplicationHelper.buildResponse(Status.BAD_REQUEST, errorResponse);
		}
	}

}
