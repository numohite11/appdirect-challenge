package com.appdirect.mychallenge.rest.response;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Response for change in subscription event (upgrade/down-grade)
 */
public class ChangeSubscriptionResponse extends EventResponse {

	public ChangeSubscriptionResponse() {
		super(true, null);
	}

	public ChangeSubscriptionResponse(final String message) {
		super(true, message);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
