package com.appdirect.mychallenge.rest.response;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Response for un-assign a user event
 */
public class UnassignUserResponse extends EventResponse {

	public UnassignUserResponse() {
		super(true, null);
	}

	public UnassignUserResponse(final String message) {
		super(true, message);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}