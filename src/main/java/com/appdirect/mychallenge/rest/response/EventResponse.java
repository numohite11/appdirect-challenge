package com.appdirect.mychallenge.rest.response;

/**
 * Abstract Response class for events
 */
public abstract class EventResponse {

	private boolean success;

	private String message;

	/**
	 * If the response is successful
	 */
	public EventResponse(final boolean success, final String message) {
		this.success = success;
		this.message = message;
	}

	protected EventResponse() {
		// For json
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessage() {
		return message;
	}

}
