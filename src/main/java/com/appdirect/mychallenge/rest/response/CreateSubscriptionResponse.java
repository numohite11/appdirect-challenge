package com.appdirect.mychallenge.rest.response;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Response for create subscription event
 */
public class CreateSubscriptionResponse extends EventResponse {

	private String accountIdentifier;

	public CreateSubscriptionResponse(final String accountIdentifier) {
		super(true, null);
		this.accountIdentifier = accountIdentifier;
	}

	public CreateSubscriptionResponse(final String accountIdentifier, final String message) {
		super(true, message);
		this.accountIdentifier = accountIdentifier;
	}

	@SuppressWarnings("unused")
	private CreateSubscriptionResponse() {
		// For json super();
	}

	/**
	 * @return the accountIdentifier
	 */
	public String getAccountIdentifier() {
		return accountIdentifier;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
