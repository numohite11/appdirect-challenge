package com.appdirect.mychallenge.rest.response;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Response for subscription status event.
 */
public class SubscriptionStatusResponse extends EventResponse {

	// TODO better to keep it as ENUM
	private String status;

	public SubscriptionStatusResponse(final String status) {
		super(true, null);
		this.status = status;
	}

	public SubscriptionStatusResponse(final String status, final String message) {
		super(true, message);
		this.status = status;
	}

	@SuppressWarnings("unused")
	private SubscriptionStatusResponse() {
		// For json super();
	}

	/**
	 * @return the status
	 */
	public String getAccountIdentifier() {
		return status;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
