package com.appdirect.mychallenge.rest.response;

/**
 * Error response for subscription event
 */
public class ErrorResponse extends EventResponse {

	private ErrorCode errorCode;

	public ErrorResponse(final ErrorCode errorCode, final String message) {
		super(false, message);
		this.errorCode = errorCode;
	}

	public ErrorResponse(final ErrorCode errorCode) {
		super(false, null);
		this.errorCode = errorCode;
	}

	@SuppressWarnings("unused")
	private ErrorResponse() {
		// For json
		super();
	}

	/**
	 * @return the errorCode
	 */
	public ErrorCode getErrorCode() {
		return errorCode;
	}

}
