package com.appdirect.mychallenge.rest.response;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Response for assign a user event
 */
public class AssignUserResponse extends EventResponse {

	public AssignUserResponse() {
		super(true, null);
	}

	public AssignUserResponse(final String message) {
		super(true, message);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}