package com.appdirect.mychallenge.rest;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import com.appdirect.mychallenge.rest.response.EventResponse;

/**
 * Helper class for application classes like AccessApplication and
 * SubscriptionApplication etc.
 */
public class ApplicationHelper {

	protected static final String EVENT_URL = "eventUrl";

	protected static Response buildResponse(final Status status, final EventResponse response) {
		ResponseBuilder builder;
		builder = Response.status(status);
		builder.type(MediaType.APPLICATION_JSON);
		builder.entity(response);
		return builder.build();
	}

}
