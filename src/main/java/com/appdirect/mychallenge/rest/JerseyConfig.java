package com.appdirect.mychallenge.rest;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

/**
 * Configuration class which registers various applications
 */
@Component
public class JerseyConfig extends ResourceConfig {

	public JerseyConfig() {
		register(AccessApplication.class);
		register(SubscriptionApplication.class);
		register(VehicleApplication.class);
	}
}
