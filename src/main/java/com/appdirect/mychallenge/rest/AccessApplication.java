package com.appdirect.mychallenge.rest;

import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.appdirect.mychallenge.rest.response.AssignUserResponse;
import com.appdirect.mychallenge.rest.response.ErrorCode;
import com.appdirect.mychallenge.rest.response.ErrorResponse;
import com.appdirect.mychallenge.rest.response.UnassignUserResponse;

/**
 * Application class to perform assign/un-assign operations on access events
 */
@Path(value = "/access")
@Component
public class AccessApplication {

	private static final Logger logger = Logger.getLogger(SubscriptionApplication.class.getName());

	/**
	 * Assign a user
	 * 
	 * @param url
	 *            eventUrl
	 * @return json response
	 */
	@GET
	@Produces("application/json")
	@Path(value = "/assign")
	public Response assignUser(@QueryParam(ApplicationHelper.EVENT_URL) final String url) {
		try {
			logger.info("Assign user called with eventUrl: " + url);
			if (StringUtils.isEmpty(url)) {
				// Mocking a scenario to return USER_NOT_FOUND
				final ErrorResponse errorResponse = new ErrorResponse(ErrorCode.USER_NOT_FOUND, "URL not passed.");
				return ApplicationHelper.buildResponse(Status.BAD_REQUEST, errorResponse);
			}
			// TODO need enhancement, for now having a NO-OP
			final AssignUserResponse response = new AssignUserResponse("User assigned successfully.");
			return ApplicationHelper.buildResponse(Status.CREATED, response);
		} catch (Exception ex) {
			logger.severe("Unexpected error occurred : " + ex);
			final ErrorResponse errorResponse = new ErrorResponse(ErrorCode.UNKNOWN_ERROR,
					"Unexpected error occurred.");
			return ApplicationHelper.buildResponse(Status.BAD_REQUEST, errorResponse);
		}
	}

	/**
	 * Unassign a user
	 * 
	 * @param url
	 *            eventUrl
	 * @return json response
	 */
	@GET
	@Produces("application/json")
	@Path(value = "/unassign")
	public Response unssignUser(@QueryParam(ApplicationHelper.EVENT_URL) final String url) {
		try {
			logger.info("Unassigning user called with eventUrl: " + url);
			if (StringUtils.isEmpty(url)) {
				// Mocking a scenario to return USER_NOT_FOUND
				final ErrorResponse errorResponse = new ErrorResponse(ErrorCode.USER_NOT_FOUND, "URL not passed.");
				return ApplicationHelper.buildResponse(Status.BAD_REQUEST, errorResponse);
			}
			// TODO need enhancement, for now having a NO-OP
			final UnassignUserResponse response = new UnassignUserResponse("User unassigned successfully.");
			return ApplicationHelper.buildResponse(Status.OK, response);
		} catch (Exception ex) {
			logger.severe("Unexpected error occurred : " + ex);
			final ErrorResponse errorResponse = new ErrorResponse(ErrorCode.UNKNOWN_ERROR,
					"Unexpected error occurred.");
			return ApplicationHelper.buildResponse(Status.BAD_REQUEST, errorResponse);
		}
	}

}
