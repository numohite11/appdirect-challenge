package com.appdirect.mychallenge.rest;

import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.appdirect.mychallenge.domain.entity.VehicleEntity;
import com.appdirect.mychallenge.rest.exception.VehicleValidationException;
import com.appdirect.mychallenge.rest.request.VehicleRequest;
import com.appdirect.mychallenge.service.VehicleServiceImpl;
import com.appdirect.mychallenge.service.exception.VehicleServiceException;

/**
 * Application class to perform various CRUD operations on vehicle
 */
@Path(value = "/myChallenge")
@Component
public class VehicleApplication {

	private static final Logger logger = Logger.getLogger(VehicleApplication.class.getName());

	private final VehicleServiceImpl vehicleServiceImpl;

	@Autowired
	public VehicleApplication(final VehicleServiceImpl vehicleServiceImpl) {
		this.vehicleServiceImpl = vehicleServiceImpl;
	}

	/**
	 * Adds a vehicle in the application
	 * 
	 * @param vehicleRequest
	 *            vehicle details
	 * @return
	 */
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	@Path(value = "/vehicles/add")
	public Response add(final VehicleRequest vehicleRequest) {
		logger.info("Add vehicle called with request: " + vehicleRequest);
		if (vehicleRequest == null) {
			return buildResponse(Status.BAD_REQUEST, new String("Request cannot be empty"));
		}
		try {
			vehicleServiceImpl.createVehicle(vehicleRequest.getMake(), vehicleRequest.getModel(),
					vehicleRequest.getManufacturingDate());
			return buildResponse(Status.CREATED, new String("Vehicle created."));
		} catch (final VehicleValidationException e) {
			return buildResponse(Status.BAD_REQUEST, new String(e.getMessage()));
		} catch (final VehicleServiceException e) {
			return buildResponse(Status.INTERNAL_SERVER_ERROR, new String("Vehicle creation failed."));
		}
	}

	/**
	 * Query a vehicle by make and model
	 * 
	 * @param make
	 *            maker of the vehicle
	 * @param model
	 *            model name of the vehicle
	 * @return
	 */
	@GET
	@Consumes("application/json")
	@Produces("application/json")
	@Path(value = "/vehicles")
	public Response query(@QueryParam("make") final String make, @QueryParam("model") final String model) {
		logger.info("Query vehicle called for make: " + make + " and model :" + model);
		try {
			final VehicleEntity vehicleEntity = vehicleServiceImpl.queryVehicle(make, model);
			return buildResponse(Status.OK, vehicleEntity);

		} catch (final VehicleValidationException e) {
			return buildResponse(Status.BAD_REQUEST, new String(e.getMessage()));
		} catch (final VehicleServiceException e) {
			return buildResponse(Status.INTERNAL_SERVER_ERROR, new String("Vehicle query failed."));
		}
	}

	/**
	 * Query a vehicle by its unique id
	 * 
	 * @param id
	 *            vehicle identifier
	 * @return
	 */
	@GET
	@Consumes("application/json")
	@Produces("application/json")
	@Path(value = "/vehicles/id/{id}")
	public Response query(@PathParam("id") final long id) {
		logger.info("Query vehicle called for id: " + id);
		try {
			final VehicleEntity vehicleEntity = vehicleServiceImpl.queryVehicle(id);
			return buildResponse(Status.OK, vehicleEntity);

		} catch (final VehicleServiceException e) {
			return buildResponse(Status.INTERNAL_SERVER_ERROR, new String("Vehicle query failed."));
		}
	}

	/**
	 * Update an existing vehicle
	 * 
	 * @param id
	 *            vehicle identifier
	 * @param vehicleRequest
	 *            details of the vehicle
	 * @return
	 */
	@PUT
	@Consumes("application/json")
	@Produces("application/json")
	@Path(value = "/vehicles/id/{id}")
	public Response update(@PathParam("id") final long id, final VehicleRequest vehicleRequest) {
		logger.info("Update vehicle called for id: " + id);
		if (vehicleRequest == null) {
			return buildResponse(Status.BAD_REQUEST, new String("Request cannot be empty"));
		}
		try {
			vehicleServiceImpl.updateVehicle(id, vehicleRequest.getMake(), vehicleRequest.getModel(),
					vehicleRequest.getManufacturingDate());
			return buildResponse(Status.OK, new String("Vehicle updated successfully."));

		} catch (final VehicleValidationException e) {
			return buildResponse(Status.BAD_REQUEST, new String(e.getMessage()));
		} catch (final VehicleServiceException e) {
			return buildResponse(Status.INTERNAL_SERVER_ERROR, new String("Update vehicle failed."));
		}
	}

	/**
	 * Delete an existing vehicle
	 * 
	 * @param id
	 *            vehicle identifier
	 * @return
	 */
	@DELETE
	@Consumes("application/json")
	@Produces("application/json")
	@Path(value = "/vehicles/id/{id}")
	public Response delete(@PathParam("id") final long id) {
		logger.info("Delete vehicle called for id: " + id);
		try {
			vehicleServiceImpl.deleteVehicle(id);
			return buildResponse(Status.OK, new String("Vehicle deleted successfully."));

		} catch (final VehicleServiceException e) {
			return buildResponse(Status.INTERNAL_SERVER_ERROR, new String("Delete vehicle failed."));
		}
	}

	private Response buildResponse(final Status status, final Object response) {
		ResponseBuilder builder;
		builder = Response.status(status);
		builder.type(MediaType.APPLICATION_JSON);
		builder.entity(response);
		return builder.build();
	}

}
