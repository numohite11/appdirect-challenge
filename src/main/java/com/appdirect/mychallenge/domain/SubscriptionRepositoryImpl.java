package com.appdirect.mychallenge.domain;

import java.sql.PreparedStatement;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.appdirect.mychallenge.domain.exception.SubscriptionDomainException;

/**
 * Implementation of subscription repository
 */
@Repository
public class SubscriptionRepositoryImpl implements SubscriptionRepository {

	private static final Logger logger = Logger.getLogger(SubscriptionRepositoryImpl.class.getName());

	private final JdbcTemplate jdbcTemplate;

	@Autowired
	public SubscriptionRepositoryImpl(final JdbcTemplate jdbc) {
		this.jdbcTemplate = jdbc;
	}

	@Override
	public int createSubscription(String companyName, String status, String marketPlaceBaseUrl)
			throws SubscriptionDomainException {
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		try {
			final String sql = "insert into subscription (company_name, status, market_place_base_url, created_date, updated_date) values (?, ?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
			jdbcTemplate.update(c -> {
				PreparedStatement ps = c.prepareStatement(sql, new String[] { "id" });
				ps.setString(1, companyName);
				ps.setString(2, status);
				ps.setString(3, marketPlaceBaseUrl);
				return ps;
			}, keyHolder);
			return keyHolder.getKey().intValue();
		} catch (final DataAccessException e) {
			logger.severe("Subscription creation failed with error message: " + e.getMessage() + "\n" + e);
			throw new SubscriptionDomainException(e.getMessage());
		}
	}

	@Override
	public boolean deleteSubscription(long id) throws SubscriptionDomainException {
		try {
			final int rows = jdbcTemplate.update("delete from subscription where id = ?", id);
			return rows == 1;
		} catch (final DataAccessException e) {
			logger.severe("Subscription deletion failed with error message: " + e.getMessage() + "\n" + e);
			throw new SubscriptionDomainException(e.getMessage());
		}
	}

	@Override
	public Long getSubscriptionId() throws SubscriptionDomainException {
		try {
			final Long subscriptionId = jdbcTemplate
					.query("select id from subscription", (rs, rowNum) -> rs.getLong("id")).get(0);
			return subscriptionId;
		} catch (final DataAccessException e) {
			logger.severe("Subscription deletion failed with error message: " + e.getMessage() + "\n" + e);
			throw new SubscriptionDomainException(e.getMessage());
		}
	}

}
