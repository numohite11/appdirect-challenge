package com.appdirect.mychallenge.domain;

import com.appdirect.mychallenge.domain.exception.UserDomainException;

/**
 * Interface for subscription repository which will have CRUD operations
 */
public interface UserRepository {

	/**
	 * creates a user
	 *
	 * @param openId
	 *            openIs
	 * @param firstName
	 *            firstName
	 * @param lastName
	 *            lastName
	 * @param subscriptionId
	 *            subscriptionId
	 * @param emailId
	 *            emailId
	 * @return user identifier
	 * @throws UserDomainException
	 *             if user creation was not successful
	 */
	public int createUserAccount(final String openId, final String firstName, final String lastName,
			final Long susbscriptionId, final String emailId) throws UserDomainException;

	/**
	 * @param subscriptionId
	 *            the subscription_id of the user we want to delete
	 * @return false if no row was deleted, true otherwise
	 * @throws UserDomainException
	 *             if error occurred during user deletion
	 */
	public boolean deleteBySubscriptionId(Long subscriptionId) throws UserDomainException;

	// TODO add other CRUD operations like update, retrieve etc

}
