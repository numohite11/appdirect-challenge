package com.appdirect.mychallenge.domain;

import java.util.Date;
import java.util.List;

import com.appdirect.mychallenge.domain.entity.VehicleEntity;
import com.appdirect.mychallenge.domain.exception.VehicleDomainException;

/**
 * Interface for vehicle repository which will have CRUD operations
 */
public interface VehicleRepository {

	/**
	 * create a vehicle
	 *
	 * @param make
	 *            makers of vehicle
	 * @param model
	 *            model name of vehicle
	 * @param manufacturingDate
	 *            manufacturing date of the vehicle
	 * @return true if vehicle creation was successful
	 * @throws VehicleDomainException
	 *             if vehicle creation was not successful
	 */
	public boolean createVehicle(final String make, final String model, final Date manufacturingDate)
			throws VehicleDomainException;

	/**
	 * query vehicle
	 *
	 * @param make
	 *            makers of vehicle
	 * @param model
	 *            model name of vehicle
	 * @return list of vehicles
	 * @throws VehicleDomainException
	 *             if vehicle query was not successful
	 */
	public List<VehicleEntity> getVehicle(String make, String model) throws VehicleDomainException;

	/**
	 * query vehicle
	 *
	 * @param id
	 *            vehicle id
	 * @return list of vehicles
	 * @throws VehicleDomainException
	 *             if vehicle query was not successful
	 */
	public List<VehicleEntity> getVehicle(final long id) throws VehicleDomainException;

	/**
	 * delete a vehicle
	 *
	 * @param id
	 *            vehicle id
	 * @return true id delete vehicle was successful
	 * @throws VehicleDomainException
	 *             if vehicle deletion was not successful
	 */
	public boolean deleteVehicle(long id) throws VehicleDomainException;

	/**
	 * update an existing vehicle
	 *
	 * @param vehicleEntity
	 *            vehicle details
	 * @return true if update vehicle was not successful
	 * @throws VehicleDomainException
	 *             if vehicle deletion was not successful
	 */
	public boolean updateVehicle(VehicleEntity vehicleEntity) throws VehicleDomainException;

}
