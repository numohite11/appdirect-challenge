package com.appdirect.mychallenge.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.appdirect.mychallenge.domain.VehicleRepository;
import com.appdirect.mychallenge.domain.entity.VehicleEntity;
import com.appdirect.mychallenge.domain.exception.VehicleDomainException;

/**
 * Implementation of vehicle repository
 */
@Repository
public class VehicleRepositoryImpl implements VehicleRepository {

	private static final Logger logger = Logger.getLogger(VehicleRepositoryImpl.class.getName());

	private final JdbcTemplate jdbcTemplate;

	@Autowired
	public VehicleRepositoryImpl(final JdbcTemplate jdbc) {
		this.jdbcTemplate = jdbc;
	}

	@Override
	public boolean createVehicle(final String make, final String model, final Date manufacturingDate)
			throws VehicleDomainException {
		try {
			final String sql = "insert into vehicle (make, model, manufacturing_date, created_date, updated_date) values (?, ?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
			final int rows = jdbcTemplate.update(sql, make, model, manufacturingDate);
			return rows == 1;
		} catch (final DataAccessException e) {
			logger.severe("Vehicle creation failed with error message: " + e.getMessage() + "\n" + e);
			throw new VehicleDomainException(e.getMessage());
		}
	}

	@Override
	public List<VehicleEntity> getVehicle(final String make, final String model) throws VehicleDomainException {
		try {
			final List<VehicleEntity> vehicleEntities = new ArrayList<VehicleEntity>();
			jdbcTemplate
					.query("select id, make, model, manufacturing_date, created_date, updated_date  from vehicle where make = ? and model = ?",
							new Object[] { make, model },
							(rs, rowNum) -> new VehicleEntity(rs.getLong("id"), rs.getString("make"),
									rs.getString("model"), rs.getTimestamp("manufacturing_date"),
									rs.getTimestamp("created_date"), rs.getTimestamp("updated_date")))
					.forEach(e -> vehicleEntities.add(e));
			return vehicleEntities;
		} catch (final DataAccessException e) {
			logger.severe("Vehicle query failed with error message: " + e.getMessage() + "\n" + e);
			throw new VehicleDomainException(e.getMessage());
		}
	}

	@Override
	public List<VehicleEntity> getVehicle(final long id) throws VehicleDomainException {
		try {
			final List<VehicleEntity> vehicleEntities = new ArrayList<VehicleEntity>();
			jdbcTemplate
					.query("select id, make, model, manufacturing_date, created_date, updated_date  from vehicle where id = ?",
							new Object[] { id },
							(rs, rowNum) -> new VehicleEntity(rs.getLong("id"), rs.getString("make"),
									rs.getString("model"), rs.getTimestamp("manufacturing_date"),
									rs.getTimestamp("created_date"), rs.getTimestamp("updated_date")))
					.forEach(e -> vehicleEntities.add(e));
			return vehicleEntities;
		} catch (final DataAccessException e) {
			logger.severe("Vehicle query failed with error message: " + e.getMessage() + "\n" + e);
			throw new VehicleDomainException(e.getMessage());
		}
	}

	@Override
	public boolean deleteVehicle(final long id) throws VehicleDomainException {
		try {
			final int rows = jdbcTemplate.update("delete from vehicle where id = ?", id);
			return rows == 1;
		} catch (final DataAccessException e) {
			logger.severe("Vehicle deletion failed with error message: " + e.getMessage() + "\n" + e);
			throw new VehicleDomainException(e.getMessage());
		}
	}

	@Override
	public boolean updateVehicle(final VehicleEntity vehicleEntity) throws VehicleDomainException {
		try {
			final String query = "update vehicle set make=?, model=?, manufacturing_date=?, updated_date=? where id=?";
			final int rows = jdbcTemplate.update(query,
					new Object[] { vehicleEntity.getMake(), vehicleEntity.getModel(),
							vehicleEntity.getManufacturingDate(), vehicleEntity.getUpdatedDate(),
							vehicleEntity.getId() });
			return rows == 1;
		} catch (final DataAccessException e) {
			logger.severe("Vehicle update failed with error message: " + e.getMessage() + "\n" + e);
			throw new VehicleDomainException(e.getMessage());
		}
	}
}
