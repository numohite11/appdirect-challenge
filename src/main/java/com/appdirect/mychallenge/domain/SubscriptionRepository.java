package com.appdirect.mychallenge.domain;

import com.appdirect.mychallenge.domain.exception.SubscriptionDomainException;

/**
 * Interface for subscription repository which will have CRUD operations
 */
public interface SubscriptionRepository {

	/**
	 * creates a subscription
	 *
	 * @param companyName
	 *            company name
	 * @param status
	 *            status of the subscription
	 * @param marketPlaceBaseUrl
	 *            marketPlaceBaseUrl
	 * @return subscription identifier
	 * @throws SubscriptionDomainException
	 *             if subscription creation was not successful
	 */
	public int createSubscription(final String companyName, final String status, final String marketPlaceBaseUrl)
			throws SubscriptionDomainException;

	/**
	 * deletes a subscription
	 *
	 * @param id
	 *            subscription id
	 * @return true id delete subscription was successful
	 * @throws SubscriptionDomainException
	 *             if subscription deletion was not successful
	 */
	public boolean deleteSubscription(long id) throws SubscriptionDomainException;

	// TODO add other CRUD operations like update, retrieve etc

	/**
	 * gets a subscriptionId if available
	 *
	 * @return subscriptionId
	 * @throws SubscriptionDomainException
	 *             if no record found
	 */
	public Long getSubscriptionId() throws SubscriptionDomainException;

}
