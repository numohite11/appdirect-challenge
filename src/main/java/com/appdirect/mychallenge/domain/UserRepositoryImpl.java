package com.appdirect.mychallenge.domain;

import java.sql.PreparedStatement;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.appdirect.mychallenge.domain.exception.UserDomainException;

/**
 * Implementation of user repository
 */
@Repository
public class UserRepositoryImpl implements UserRepository {

	private static final Logger logger = Logger.getLogger(UserRepositoryImpl.class.getName());

	private final JdbcTemplate jdbcTemplate;

	@Autowired
	public UserRepositoryImpl(final JdbcTemplate jdbc) {
		this.jdbcTemplate = jdbc;
	}

	@Override
	public int createUserAccount(String openId, String firstName, String lastName, Long susbscriptionId, String emailId)
			throws UserDomainException {
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		try {
			final String sql = "insert into user_account (open_id, first_name, last_name, subscription_id, email_id) values (?, ?, ?, ?, ?)";
			jdbcTemplate.update(c -> {
				PreparedStatement ps = c.prepareStatement(sql, new String[] { "id" });
				ps.setString(1, openId);
				ps.setString(2, firstName);
				ps.setString(3, lastName);
				ps.setLong(4, susbscriptionId);
				ps.setString(5, emailId);
				return ps;
			}, keyHolder);
			return keyHolder.getKey().intValue();
		} catch (final DataAccessException e) {
			logger.severe("User creation failed with error message: " + e.getMessage() + "\n" + e);
			throw new UserDomainException(e.getMessage());
		}
	}

	@Override
	public boolean deleteBySubscriptionId(Long subscriptionId) throws UserDomainException {
		try {
			final int rows = jdbcTemplate.update("delete from user_account where subscription_id = ?", subscriptionId);
			return rows == 1;
		} catch (final DataAccessException e) {
			logger.severe("Subscription deletion failed with error message: " + e.getMessage() + "\n" + e);
			throw new UserDomainException(e.getMessage());
		}
	}

}
