package com.appdirect.mychallenge.domain.exception;

/**
 * Domain exception thrown when vehicle CRUD operation fails
 */
public class VehicleDomainException extends Exception {

	private static final long serialVersionUID = -3181549717779268146L;

	/**
	 * Domain exception thrown when vehicle CRUD operation fails
	 * 
	 * @param message
	 */
	public VehicleDomainException(final String message) {
		super(message);
	}

}