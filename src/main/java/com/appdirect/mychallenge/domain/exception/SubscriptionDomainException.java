package com.appdirect.mychallenge.domain.exception;

/**
 * Domain exception thrown when subscription CRUD operation fails
 */
public class SubscriptionDomainException extends Exception {

	private static final long serialVersionUID = 3304636807445613493L;

	/**
	 * Domain exception thrown when subscription CRUD operation fails
	 * 
	 * @param message
	 */
	public SubscriptionDomainException(final String message) {
		super(message);
	}

}