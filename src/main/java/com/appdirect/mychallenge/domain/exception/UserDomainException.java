package com.appdirect.mychallenge.domain.exception;

/**
 * Domain exception thrown when user CRUD operation fails
 */
public class UserDomainException extends Exception {

	private static final long serialVersionUID = 6497327499941641405L;

	/**
	 * Domain exception thrown when user CRUD operation fails
	 * 
	 * @param message
	 */
	public UserDomainException(final String message) {
		super(message);
	}

}