package com.appdirect.mychallenge.domain.entity;

import java.util.Date;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Subscription Entity
 */
public class SubscriptionEntity {

	private long id;

	private String companyName;

	private String status;

	private String marketPlaceBaseUrl;

	private Date createdDate;

	private Date updatedDate;

	public SubscriptionEntity(final long id, final String companyName, final String status,
			final String marketPlaceBaseUrl, final Date createdDate, final Date updatedDate) {
		super();
		this.id = id;
		this.companyName = companyName;
		this.status = status;
		this.marketPlaceBaseUrl = marketPlaceBaseUrl;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
	}

	@SuppressWarnings("unused")
	private SubscriptionEntity() {
		// For tests
	}

	public long getId() {
		return id;
	}

	public void setId(final long id) {
		this.id = id;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(final String companyName) {
		this.companyName = companyName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	public String getMarketPlaceBaseUrl() {
		return marketPlaceBaseUrl;
	}

	public void setMarketPlaceBaseUrl(final String marketPlaceBaseUrl) {
		this.marketPlaceBaseUrl = marketPlaceBaseUrl;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(final Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(final Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Override
	public boolean equals(final Object obj) {
		return EqualsBuilder.reflectionEquals(obj, this);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
}