package com.appdirect.mychallenge.domain.entity;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * User account entity
 */
public class UserAccountEntity {

	private Long id;
	private String openId;
	private String firstname;
	private String lastname;
	private String email;
	private Long subscriptionId;

	@SuppressWarnings("unused")
	private UserAccountEntity() {
	}

	public UserAccountEntity(final Long id, final String openId, final String firstname, final String lastname,
			final String email, final Long subscriptionId) {
		this.id = id;
		this.openId = openId;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.subscriptionId = subscriptionId;
	}

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(final String openId) {
		this.openId = openId;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(final String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(final String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(final Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	@Override
	public boolean equals(final Object obj) {
		return EqualsBuilder.reflectionEquals(obj, this);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

}