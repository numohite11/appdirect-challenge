package com.appdirect.mychallenge.domain.entity;

import java.util.Date;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Vehicle Entity
 */
public class VehicleEntity {

	private long id;

	private String make;

	private String model;

	private Date manufacturingDate;

	private Date createdDate;

	private Date updatedDate;

	public VehicleEntity(final long id, final String make, final String model, final Date manufaturingDate,
			final Date createdDate, final Date updatedDate) {
		this.id = id;
		this.make = make;
		this.model = model;
		this.manufacturingDate = manufaturingDate;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
	}

	public VehicleEntity(final String make, final String model) {
		this.make = make;
		this.model = model;
	}

	@SuppressWarnings("unused")
	private VehicleEntity() {
		// For tests
	}

	public long getId() {
		return id;
	}

	public void setId(final long id) {
		this.id = id;
	}

	public String getMake() {
		return make;
	}

	public void setMake(final String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(final String model) {
		this.model = model;
	}

	public Date getManufacturingDate() {
		return manufacturingDate;
	}

	public void setManufacturingDate(final Date manufacturingDate) {
		this.manufacturingDate = manufacturingDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(final Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(final Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Override
	public boolean equals(final Object obj) {
		return EqualsBuilder.reflectionEquals(obj, this);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

}
