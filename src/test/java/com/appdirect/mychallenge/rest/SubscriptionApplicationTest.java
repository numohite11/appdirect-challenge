package com.appdirect.mychallenge.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import com.appdirect.mychallenge.MyChallenge;
import com.appdirect.mychallenge.rest.response.CancelSubscriptionResponse;
import com.appdirect.mychallenge.rest.response.ChangeSubscriptionResponse;
import com.appdirect.mychallenge.rest.response.CreateSubscriptionResponse;
import com.appdirect.mychallenge.rest.response.ErrorCode;
import com.appdirect.mychallenge.rest.response.ErrorResponse;

/**
 * Tests for SubscriptionApplication
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MyChallenge.class)
@WebAppConfiguration
@IntegrationTest
public class SubscriptionApplicationTest {

	private static String CREATE_URL = "http://localhost:8443/subscription/create";

	private static String CANCEL_URL = "http://localhost:8443/subscription/cancel";

	private static String CHANGE_URL = "http://localhost:8443/subscription/change";

	private static String STATUS_URL = "http://localhost:8443/subscription/status";

	private RestTemplate restTemplate = new TestRestTemplate();

	/**
	 * Test create subscription by passing a url
	 * 
	 * @throws Exception
	 */
	@Test
	public void testCreateSubscription() throws Exception {
		CreateSubscriptionResponse response = restTemplate.getForObject(CREATE_URL + "?eventUrl=http://createurl.com",
				CreateSubscriptionResponse.class);
		assertNotNull("Create Subscription Response cannot be null.", response);
		assertEquals("Subscription successfully created.", response.getMessage());
		assertTrue(response.isSuccess());
		assertNotNull("AccountIdentifier cannot be null.", response.getAccountIdentifier());
	}

	/**
	 * Test cancel subscription by passing a url
	 * 
	 * @throws Exception
	 */
	@Test
	public void testCancelSubscription() throws Exception {
		//create a subscription first
		CreateSubscriptionResponse response = restTemplate.getForObject(CREATE_URL + "?eventUrl=http://createurl.com",
				CreateSubscriptionResponse.class);
		assertNotNull("Create Subscription Response cannot be null.", response);
		assertEquals("Subscription successfully created.", response.getMessage());
		assertTrue(response.isSuccess());
		assertNotNull("AccountIdentifier cannot be null.", response.getAccountIdentifier());
		//cancel a subscription
		CancelSubscriptionResponse cancelResponse = restTemplate.getForObject(CANCEL_URL + "?eventUrl=http://cancelurl.com",
				CancelSubscriptionResponse.class);
		assertNotNull("Cancel Subscription Response cannot be null.", cancelResponse);
		assertEquals("Subscription successfully canceled.", cancelResponse.getMessage());
		assertTrue(cancelResponse.isSuccess());
	}

	/**
	 * Test change subscription by passing a url
	 * 
	 * @throws Exception
	 */
	@Test
	public void testChangeSubscription() throws Exception {
		ChangeSubscriptionResponse response = restTemplate.getForObject(CHANGE_URL + "?eventUrl=http://changeurl.com",
				ChangeSubscriptionResponse.class);
		assertNotNull("Change Subscription Response cannot be null.", response);
		assertEquals("Subscription successfully changed.", response.getMessage());
		assertTrue(response.isSuccess());
	}

	/**
	 * Test get status of subscription by passing a url
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetSubscriptionStatus() throws Exception {
		CreateSubscriptionResponse response = restTemplate.getForObject(STATUS_URL + "?eventUrl=http://createurl.com",
				CreateSubscriptionResponse.class);
		assertNotNull("Get subscription status response cannot be null.", response);
		assertEquals("Subscription is in ACTIVE state.", response.getMessage());
		assertTrue(response.isSuccess());
		assertNotNull("AccountIdentifier cannot be null.", response.getAccountIdentifier());
	}

	/**
	 * Test subscription events without url
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSubscriptionEventsWithInvalidRedirectUrl() throws Exception {
		List<String> urls = Arrays.asList(CREATE_URL, CANCEL_URL, CHANGE_URL, STATUS_URL);
		urls.forEach(url -> testSubscriptionEventsWithInvalidRedirectUrl(url));
	}

	private void testSubscriptionEventsWithInvalidRedirectUrl(String url) {
		ErrorResponse response = restTemplate.getForObject(url, ErrorResponse.class);
		assertNotNull("Get subscription status response cannot be null.", response);
		assertEquals("URL not passed.", response.getMessage());
		assertFalse(response.isSuccess());
		assertEquals(ErrorCode.USER_NOT_FOUND, response.getErrorCode());
	}
}
