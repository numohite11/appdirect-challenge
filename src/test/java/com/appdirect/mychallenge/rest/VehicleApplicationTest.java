package com.appdirect.mychallenge.rest;

import org.junit.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.junit.Assert.*;
import org.springframework.web.client.RestTemplate;

import com.appdirect.mychallenge.MyChallenge;
import com.appdirect.mychallenge.domain.entity.VehicleEntity;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Tests for VehicleApplication
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MyChallenge.class)
@WebAppConfiguration
@IntegrationTest
public class VehicleApplicationTest {

	private static String URL = "http://localhost:8443/myChallenge/vehicles";

	private ObjectMapper objectMapper = new ObjectMapper();

	private RestTemplate restTemplate = new TestRestTemplate();

	/**
	 * Test create vehicle by passing make and model and subsequent CRUD
	 * operations
	 * 
	 * @throws Exception
	 */
	@Test
	public void testCreateVehicle() throws Exception {
		final Date date = new Date();
		// Add vehicle
		Map<String, Object> requestBody = new HashMap<String, Object>();
		requestBody.put("make", "BMW");
		requestBody.put("model", "320D");
		requestBody.put("manufacturingDate", date);
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> httpEntity = new HttpEntity<String>(objectMapper.writeValueAsString(requestBody),
				requestHeaders);
		String addResponse = restTemplate.postForObject(URL + "/add", httpEntity, String.class);
		assertNotNull("Add vehicle response has to be non-null.", addResponse);
		assertEquals("Vehicle created.", addResponse);

		// query vehicle by id
		VehicleEntity getResponse = restTemplate.getForObject(URL + "/id/1", VehicleEntity.class);
		assertNotNull("Get vehicle response cannot be null.", getResponse);
		assertEquals(1, getResponse.getId());
		assertEquals("BMW", getResponse.getMake());
		assertEquals("320D", getResponse.getModel());
		assertEquals(date, getResponse.getManufacturingDate());

		// query vehicle by make and model
		VehicleEntity getResponse1 = restTemplate.getForObject(URL + "?make=BMW&model=320D", VehicleEntity.class);
		assertNotNull("Get vehicle response cannot be null.", getResponse1);
		assertEquals(getResponse, getResponse1);

		// update vehicle
		final Date newDate = new Date();
		Map<String, Object> updateBody = new HashMap<String, Object>();
		updateBody.put("make", "BMW_New");
		updateBody.put("model", "320D_New");
		updateBody.put("manufacturingDate", newDate);
		restTemplate.put(URL + "/id/1", updateBody, String.class);

		// query vehicle by id
		VehicleEntity getUpdatedVehicle = restTemplate.getForObject(URL + "/id/1", VehicleEntity.class);
		assertNotNull("Get vehicle response cannot be null.", getResponse);
		assertEquals(1, getUpdatedVehicle.getId());
		assertEquals("BMW_New", getUpdatedVehicle.getMake());
		assertEquals("320D_New", getUpdatedVehicle.getModel());
		assertEquals(newDate, getUpdatedVehicle.getManufacturingDate());

		// delete vehicle
		restTemplate.delete(URL + "/id/1");

		// query vehicle by id
		String getDeletedVehicle = restTemplate.getForObject(URL + "/id/1", String.class);
		assertEquals("Vehicle query failed.", getDeletedVehicle);
	}
}
