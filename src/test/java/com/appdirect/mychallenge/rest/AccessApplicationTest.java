package com.appdirect.mychallenge.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import com.appdirect.mychallenge.MyChallenge;
import com.appdirect.mychallenge.rest.response.AssignUserResponse;
import com.appdirect.mychallenge.rest.response.ErrorCode;
import com.appdirect.mychallenge.rest.response.ErrorResponse;
import com.appdirect.mychallenge.rest.response.UnassignUserResponse;

/**
 * Tests for AccessApplication
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MyChallenge.class)
@WebAppConfiguration
@IntegrationTest
public class AccessApplicationTest {

	private static String ASSIGN_URL = "http://localhost:8443/access/assign";

	private static String UNASSIGN_URL = "http://localhost:8443/access/unassign";

	private RestTemplate restTemplate = new TestRestTemplate();

	/**
	 * Test assign user by passing a url
	 * 
	 * @throws Exception
	 */
	@Test
	public void testAssignUserHappyPath() throws Exception {
		AssignUserResponse response = restTemplate.getForObject(ASSIGN_URL + "?eventUrl=http://assignurl.com",
				AssignUserResponse.class);
		assertNotNull("Assign user response cannot be null.", response);
		assertEquals("User assigned successfully.", response.getMessage());
		assertTrue(response.isSuccess());
	}

	/**
	 * Test assign user by not passing a url
	 * 
	 * @throws Exception
	 */
	@Test
	public void testAssignUserWithoutUrl() throws Exception {
		ErrorResponse response = restTemplate.getForObject(ASSIGN_URL, ErrorResponse.class);
		assertNotNull("Assign user response cannot be null.", response);
		assertEquals("URL not passed.", response.getMessage());
		assertFalse(response.isSuccess());
		assertEquals(ErrorCode.USER_NOT_FOUND, response.getErrorCode());
	}

	/**
	 * Test un-assign user by passing a url
	 * 
	 * @throws Exception
	 */
	@Test
	public void testUnAssignUserHappyPath() throws Exception {
		UnassignUserResponse response = restTemplate.getForObject(UNASSIGN_URL + "?eventUrl=http://unassignurl.com",
				UnassignUserResponse.class);
		assertNotNull("Unassign user response cannot be null.", response);
		assertEquals("User unassigned successfully.", response.getMessage());
		assertTrue(response.isSuccess());
	}

	/**
	 * Test un-assign user by not passing a url
	 * 
	 * @throws Exception
	 */
	@Test
	public void testUnAssignUserWithoutUrl() throws Exception {
		ErrorResponse response = restTemplate.getForObject(UNASSIGN_URL, ErrorResponse.class);
		assertNotNull("Unassign user response cannot be null.", response);
		assertEquals("URL not passed.", response.getMessage());
		assertFalse(response.isSuccess());
		assertEquals(ErrorCode.USER_NOT_FOUND, response.getErrorCode());
	}

}
